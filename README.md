# OpenML dataset: Ethereum-Cryptocurrency-Historical-Dataset

https://www.openml.org/d/43391

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Ethereum a decentralized, open-source blockchain featuring smart contract functionality was proposed in 2013 by programmer Vitalik Buterin. Development was crowdfunded in 2014, and the network went live on 30 July 2015, with 72 million coins premined. 
Some interesting facts about Ethereum(ETH):

Ether (ETH) is the native cryptocurrency of the platform. It is the second-largest cryptocurrency by market capitalization, after Bitcoin. Ethereum is the most actively used blockchain.
Some of the worlds leading corporations joined the EEA(Ethereum Alliance, is a collaboration of many block start-ups) and supported further development. Some of the most famous companies are Samsung SDS, Toyota Research Institute, Banco Santander, Microsoft, J.P.Morgan, Merck GaA, Intel, Deloitte, DTCC, ING, Accenture, Consensys, Bank of Canada, and BNY Mellon.

Content
The dataset consists of ETH prices from March-2016 to the current date(1813 days) and the dataset will be updated on a weekly basis. 
Information regarding the data
The data totally consists of 1813 records(1813 days) with 7 columns. The description of the features is given below



No
Columns
Descriptions




1
Date
Date of the ETH prices


2
Price
Prices of ETH(dollars)


3
Open
Opening price of ETH on the respective date(Dollars)


4
High
Highest price of ETH on the respective date(Dollars)


5
Low
Lowest price of ETH on the respective date(Dollars)


6
Vol.
Volume of ETH on the respective date(Dollars).


7
Change 
Percentage of Change in ETH prices on the respective date



Acknowledgements
The dataset was extracted from investing.com
Inspiration
Experts say that ethereum has a huge potential in the future. Do you believe it? Well, let's find it by building our own creative models to predict if the statement is true.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43391) of an [OpenML dataset](https://www.openml.org/d/43391). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43391/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43391/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43391/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

